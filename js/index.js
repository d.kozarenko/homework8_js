"use strict";

const input = document.querySelector("#input");
const span = document.createElement("span");
const button = document.createElement("button");
const alert = document.createElement("span");
input.after(alert);
input.addEventListener("focus", inputFocusingHandler);
input.addEventListener("blur", inputBluringHandler);

function inputFocusingHandler() {
  input.classList.add("input");
}
function createPriceBlock() {
  button.classList.add("button");
  span.classList.add("span");
  input.before(span);
  span.textContent = `Текущая цена: ${event.target.value}$`;
  span.after(button);
  button.textContent = `X`;
}
function inputBluringHandler(event) {
  if (
    typeof parseInt(event.target.value) === `number` &&
    parseInt(event.target.value) > 0 &&
    !isNaN(event.target.value)
  ) {
    hideError();
    createPriceBlock();
    showPriceBlock();
    input.addEventListener("keyup", () => {
      span.textContent = `Текущая цена: ${event.target.value}$`;
    });
    event.target.classList.add("green-font");
    button.addEventListener("click", () => {
      button.remove();
      span.remove();
      event.target.classList.remove(`green-font`);
      event.target.value = "";
    });
  } else {
    createShowError();
  }
}
function removePriceBlock() {
  button.classList.add("none");
  span.classList.add("none");
}
function createShowError() {
  if (button && span) {
    removePriceBlock();
  }
  alert.textContent = "Please enter correct price";
  input.classList.remove("input");
  input.classList.add("input-error");
  input.classList.remove("green-font");
}
function hideError() {
  input.classList.remove("input-error");
  alert.textContent = "";
}
function showPriceBlock() {
  button.classList.remove("none");
  span.classList.remove("none");
  button.classList.add("inline-block");
  span.classList.add("inline-block");
}
